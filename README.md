# weekly competitions
The dataset gives you information about a marketing campaign of a financial institution in which you will have
to analyze in order to find ways to look for future strategies in order to improve future marketing campaigns for the bank.

Your first task is to explore the data and do some visualizations.

Dataset is attached - bank.csv